import React, { Component } from 'react'

export default class DisplayPhone extends Component {

    render() {
        let { displayPhone } = this.props;
        return (
            <div>
                <h1>Display Phone</h1>
                <p>{displayPhone}</p>

                
            </div>
        )
    }
}
