import React from 'react'

export default function FunctionalComponent() {
    return (
        <div>
            Hello,this is an example of a functional component!
        </div>
    )
}
