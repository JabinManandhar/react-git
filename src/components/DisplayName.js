import React, { Component } from "react";

export default class DisplayName extends Component{
    render() {
        let { displayName } = this.props;
        return (
            <div>
                <h1>Display Name</h1>
                <p>{displayName}</p>
            </div>
        )
    }
}
