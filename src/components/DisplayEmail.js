import React, { Component } from 'react'

export default class DisplayEmail extends Component {

    render() {
        let { displayEmail } = this.props;
        return (
            <div>
                <h1>Display Email:
                 <span>
                        {displayEmail}
                 </span>
                </h1>
            </div>
        )
    }
}
