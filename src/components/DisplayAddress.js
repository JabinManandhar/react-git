import React, { Component } from "react"

export default class DisplayAddress extends Component{
    render() {
        let { displayAddress } = this.props;
        return (
            <div>
                <h1>Display Address</h1>
                <p>{displayAddress}</p>
            </div>
        )
    }
}

