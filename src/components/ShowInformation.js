import React, { Component } from "react"
import DisplayName from "./DisplayName";
import DisplayAddress from "./DisplayAddress";
import DisplayPhone from "./DisplayPhone";
import DisplayEmail from "./DisplayEmail";

export default class ShowInformation extends Component {
    render() {
        let { full_name, myEmail, myAddress, phoneNumber } = this.props;
        return (
            <div>
                <h1>From child compoonent</h1>
                <p>Your name is:{full_name}</p>
                <p>Your email is:{myEmail}</p>
                <p>Your Number is:{phoneNumber}</p>
                <p>Your address is:{myAddress}</p>
                <DisplayName
                    displayName={full_name} />
                <DisplayEmail
                    displayEmail={myEmail} />
                <DisplayPhone
                    displayPhone={phoneNumber} />
                <DisplayAddress
                    displayAddress={myAddress} />
            </div>
        )
    }
}