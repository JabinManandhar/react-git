import React, { Component } from 'react';
import '../src/App.css';
import ShowInformation from './components/ShowInformation';

export default class App extends Component {
  state = {
    name: 'Jabin',
    email: 'manandhar356@gmail.com',
    phone_number: 9861184331,
    address: 'Kirtipur'
  }
  render() {
    let { name, email } = this.state;
    return (
      <div>
        {/* Using State  */}
        {/* <h1 style={{ color: "green" }}>Hello {name}!</h1>
        <h3 style={{ color: "blueviolet" }}>Email:{email}</h3>
        <h2 style={{ color: "purple" }}>Phone:{phone_number}</h2>
        <h3 style={{ color: "black" }}>Address:{address}</h3> */}
        <ShowInformation
          full_name={name}      //Using name from state object
          myEmail={email}
          phoneNumber={9841881306}  //Manually entering value as props for ShowInformation
          myAddress='Kathmandu'
        />
      </div>
    )
  }
}
